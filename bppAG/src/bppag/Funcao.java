/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bppag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Felipe
 */
public class Funcao {

    public Funcao (){}
    
    /**
     * first fit decreasing algorithm for bin packing problem
     * @param elementos
     * @param caixas
     * @param tamanhoCaixa
     * @return 
     */
    public int calculaAptidao(Integer[] elementos, ArrayList<Caixa> caixas, int tamanhoCaixa) {
        Arrays.sort(elementos, Collections.reverseOrder()); // sort input by size (big to small)
        caixas.add(new Caixa(tamanhoCaixa)); // add first bin
        
        for (Integer itemAtual : elementos) {
            
            // iterate over bins and try to put the item into the first one it fits into
            boolean colocouItem = false; // did we put the item in a bin?
            int caixaAtual = 0;
            
            while (!colocouItem) {
                //System.out.println(Arrays.toString(elementos));
                if (caixaAtual == caixas.size()) {
                    // item did not fit in last bin. put it in a new bin
                    Caixa novaCaixa = new Caixa(tamanhoCaixa);
                    novaCaixa.colocaItem(itemAtual);
                    caixas.add(novaCaixa);
                    colocouItem = true;
                } else if (caixas.get(caixaAtual).colocaItem(itemAtual)) {
                    // item fit in bin
                    colocouItem = true;
                } else {
                    // try next bin
                    caixaAtual++;
                }
            }
        }
        return caixas.size();
    }
    
    public void mostraCaixas(Integer[] elementos, ArrayList<Caixa> caixas){
        System.out.println("Itens Usados:");
           if (caixas.size() == elementos.length) {
             //System.out.println("each item is in its own bin");
           } else {
               for (Caixa caixa : caixas) {
                     System.out.println("  Caixa " + caixas.indexOf(caixa) + ":" + caixa.itensUsados);
                  }
           }  
    }
      
    
}
