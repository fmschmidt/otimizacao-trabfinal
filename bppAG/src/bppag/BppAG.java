/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bppag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author Felipe
 */
public class BppAG {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Integer[] elementos = {5,10,20,30,15};  //genes ou individuos?
        int tamanhoCaixa = 50;
        int numeroItens;
        //int caixas;
        double taxaDeCrossover = 0.6;
        double taxaDeMutacao = 0.3;
        
        ArrayList<Caixa> caixas = new ArrayList<>();
        Funcao funcao = new Funcao();
        
        boolean temSolucao = false;
        int geracao = 0;
        
        //tamanho da população = numero itens (individuos) = primeira linha do arquivo
        int tamPop = 5;
        //numero máximo de gerações = um dos criterios de parada
        int numMaxGeracoes = 1000;
        
        int aptidao = funcao.calculaAptidao(elementos, caixas, tamanhoCaixa);
        
        //cria a primeira população aleatérioa
        Populacao populacao = new Populacao(elementos, tamPop);
        
        
        System.out.println("Tamanho Máximo: " + tamanhoCaixa); 
        System.out.println(Arrays.toString(elementos));
        System.out.println("Numero de caixas: " + aptidao); 
        
        funcao.mostraCaixas(elementos, caixas);
        

        //população inicial
        //avalia
        //seleciona
        //cruza
        //mutaçao
        //atualização
        // < finalizaçao >
        // se finalizou, soluçã final
        // se nao finalizou, avalia de novo
        
        /* 
            Seja S(t) a população de cromossomos na geração t. // S
            t ← 0                                // t = 0
            inicializar S(t)                     // inicia_populaçao (S, t)
            avaliar S(t)                         // avalia (S, t)
            
            enquanto o critério de parada não for
            satisfeito faça
             t ← t+ 1                             // t++
             selecionar S(t) a partir de S(t- 1)  // seleção_dos_pais (S, t) 
             aplicar crossover sobre S(t)         // recombinação(S,t)
             aplicar mutação sobre S(t)           // avaliação(S,t)
             avaliar S(t)                         // sobrevivem(S,t)
            fim enquanto
        
        */
        
    }
        
}
