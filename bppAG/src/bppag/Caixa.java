/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bppag;

import java.util.ArrayList;

/**
 *
 * @author Felipe
 */
public class Caixa {        // solução. cromossomos.
    
    public int tamanhoAtual;
    public int tamanhoMaximo;
    public ArrayList<Integer> itensUsados;      //individuos
    
    public Caixa(int tamanhoMaximo) {
        this.tamanhoMaximo = tamanhoMaximo;
        this.tamanhoAtual = 0;
        this.itensUsados = new ArrayList<>();
    }
    
    public boolean colocaItem(int item){
      if (tamanhoAtual + item <= tamanhoMaximo) {
            itensUsados.add(item);
            tamanhoAtual += item;
            return true;
        } else {
            return false; // item didn't fit
        }
    }
     
}
