/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testag;

/** 
 * Esta classe tem o objetivo de prover a abstração necessaria para o funcionamento 
 * de um casal de individuos. 
  
 */  
public class Casal {  
     
   private Individuo pai;  
   private Individuo mae;  
     
   /** 
    *  construtor. 
    * @param pai individuo 1. 
    * @param mae individuo 2. 
    */  
   public Casal(Individuo pai, Individuo mae) {  
      this.pai = pai;  
      this.mae = mae;  
   }  
     
   /** 
    *  realiza o cruzamento entre o casal. 
    * @param taxaDeMutacao taxa de mutacao. 
    * @return filho gerado do cruzamento. 
    */  
   public Individuo cruzar(double taxaDeMutacao) {  
      return pai.cruzar(mae, taxaDeMutacao);  
   }  
}  
