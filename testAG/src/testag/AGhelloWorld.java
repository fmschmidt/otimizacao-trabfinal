package testag;

public class AGhelloWorld {

    public static void main(String[] args) {
        final int NUMERO_DE_GERACOES = 15;
        final double TX_CRUZAMENTO = 0.1;
        final double TX_MUTACAO = 0.01;

         System.out.println("Inicio do experimento! (processando...) ");  
         Populacao p = new Populacao(10);  
         for (int i = 0; i < NUMERO_DE_GERACOES; i++) {  
            System.out.println("geracao: " + i + "\n");  
            p.novaGeracao(TX_CRUZAMENTO, TX_MUTACAO);  
            System.out.println(p.toString());  
         }  
         System.out.println("Fim do experimento!");  
        
    }
}